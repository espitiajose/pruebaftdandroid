package com.farmatodo.pruebafarmatodo.restApi;

public class ListResponse {

    private int code;
    private String copyright;
    private String attributionText;

    public ListResponse(int code, String copyright, String attributionText) {
        this.code = code;
        this.copyright = copyright;
        this.attributionText = attributionText;
    }

    public int getCode() {
        return code;
    }

    public String getCopyright() {
        return copyright;
    }

    public String getAttributionText() {
        return attributionText;
    }
}
