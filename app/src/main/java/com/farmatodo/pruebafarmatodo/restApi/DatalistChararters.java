package com.farmatodo.pruebafarmatodo.restApi;

import com.farmatodo.pruebafarmatodo.models.Character;
import com.farmatodo.pruebafarmatodo.models.Relationship;

import java.util.ArrayList;

public class DatalistChararters extends Datalist {

    private ArrayList<Character> results;

    public DatalistChararters(int offset, int limit, int total, int count, ArrayList<Character> results) {
        super(offset, limit, total, count);
        this.results = results;
    }

    public ArrayList<Character> getResults() {
        return results;
    }
}
