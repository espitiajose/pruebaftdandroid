package com.farmatodo.pruebafarmatodo.restApi;
import com.farmatodo.pruebafarmatodo.models.Comic;
import java.util.ArrayList;

public class DataListComics extends Datalist {

    private ArrayList<Comic> results;

    public DataListComics(int offset, int limit, int total, int count, ArrayList<Comic> results) {
        super(offset, limit, total, count);
        this.results = results;
    }

    public ArrayList<Comic> getResults() {
        return results;
    }
}
