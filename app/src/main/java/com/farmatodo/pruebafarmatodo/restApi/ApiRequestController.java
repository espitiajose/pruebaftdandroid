package com.farmatodo.pruebafarmatodo.restApi;


import retrofit2.Retrofit;

public class ApiRequestController {

    private static ApiRequestController apiRequestController = null;

    private ApiInterface apiInterface;
    public static ApiRequestController getInstance() {
        if (apiRequestController == null) {
            apiRequestController = new ApiRequestController();
        }

        return apiRequestController;
    }

    private ApiRequestController(){

        Retrofit retrofit = ApiClient.getClient();
        apiInterface = retrofit.create(ApiInterface.class);
    }

    public ApiInterface getApiInterface() {
        return apiInterface;
    }




}
