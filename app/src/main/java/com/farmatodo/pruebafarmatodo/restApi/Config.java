package com.farmatodo.pruebafarmatodo.restApi;


import java.util.HashMap;
import java.util.Map;

public class Config {
    static final String BASE_URL = "https://gateway.marvel.com:443";

    static public Map<String, String> getMap(){
        Map<String, String> params = new HashMap<>();
        params.put("hash", "bf7490ddfc96510d0145c52fa89a17d2");
        params.put("ts", "1");
        params.put("apikey", "6fc9b9b7fd88ad43ff7807bbb6bc0d30");

        return params;
    }
}
