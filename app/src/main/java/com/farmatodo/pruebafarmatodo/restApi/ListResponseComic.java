package com.farmatodo.pruebafarmatodo.restApi;

public class ListResponseComic extends ListResponse {

    private DataListComics data;

    public ListResponseComic(int code, String copyright, String attributionText, DataListComics data) {
        super(code, copyright, attributionText);
        this.data = data;
    }

    public DataListComics getData() {
        return data;
    }
}
