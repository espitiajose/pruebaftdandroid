package com.farmatodo.pruebafarmatodo.restApi;

public class ListResponseCharacter extends ListResponse {

    private DatalistChararters data;

    public ListResponseCharacter(int code, String copyright, String attributionText, DatalistChararters data) {
        super(code, copyright, attributionText);
        this.data = data;
    }

    public DatalistChararters getData() {
        return data;
    }



}
