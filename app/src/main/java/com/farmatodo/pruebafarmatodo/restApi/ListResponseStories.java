package com.farmatodo.pruebafarmatodo.restApi;

public class ListResponseStories extends ListResponse {

    private DataListStories data;

    public ListResponseStories(int code, String copyright, String attributionText, DataListStories data) {
        super(code, copyright, attributionText);
        this.data = data;
    }

    public DataListStories getData() {
        return data;
    }

}
