package com.farmatodo.pruebafarmatodo.restApi;

public class Datalist {

    private int offset;
    private int limit;
    private int total;
    private int count;

    public Datalist(int offset, int limit, int total, int count) {
        this.offset = offset;
        this.limit = limit;
        this.total = total;
        this.count = count;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }

    public int getTotal() {
        return total;
    }

    public int getCount() {
        return count;
    }
}
