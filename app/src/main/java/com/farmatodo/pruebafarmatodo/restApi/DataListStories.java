package com.farmatodo.pruebafarmatodo.restApi;

import com.farmatodo.pruebafarmatodo.models.Comic;
import com.farmatodo.pruebafarmatodo.models.Storie;

import java.util.ArrayList;

public class DataListStories extends Datalist {

    private ArrayList<Storie> results;

    public DataListStories(int offset, int limit, int total, int count, ArrayList<Storie> results) {
        super(offset, limit, total, count);
        this.results = results;
    }

    public ArrayList<Storie> getResults() {
        return results;
    }
}
