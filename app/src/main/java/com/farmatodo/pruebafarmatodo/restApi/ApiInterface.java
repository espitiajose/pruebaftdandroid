package com.farmatodo.pruebafarmatodo.restApi;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @GET("/v1/public/characters")
    Call<ListResponseCharacter> characters(@QueryMap Map<String, String> params);

    @GET("/v1/public/characters/{id}")
    Call<ListResponseCharacter> getCharacter(@QueryMap Map<String, String> params, @Path(value = "id", encoded = true) String id);

    @GET("/v1/public/comics")
    Call<ListResponseComic> comics(@QueryMap Map<String, String> params);

    @GET("/v1/public/comics/{id}")
    Call<ListResponseComic> getComic(@QueryMap Map<String, String> params, @Path(value = "id", encoded = true) String id);

    @GET("/v1/public/stories")
    Call<ListResponseStories> stories(@QueryMap Map<String, String> params);

    @GET("/v1/public/stories/{id}")
    Call<ListResponseStories> getStorie(@QueryMap Map<String, String> params, @Path(value = "id", encoded = true) String id);

}
