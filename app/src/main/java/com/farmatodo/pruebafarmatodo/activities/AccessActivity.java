package com.farmatodo.pruebafarmatodo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.farmatodo.pruebafarmatodo.R;
import com.farmatodo.pruebafarmatodo.models.Calculator.Calculator;

public class AccessActivity extends AppCompatActivity {

    private EditText mOperation;
    private Button mBtnCalcuate;
    private TextView mTextResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access);
        mOperation = findViewById(R.id.operation);
        mBtnCalcuate = findViewById(R.id.btnCalculate);
        mTextResult = findViewById(R.id.textResult);

        mBtnCalcuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    Calculator calculator = new Calculator();
                    String result = null;
                    Intent intent = new Intent(AccessActivity.this, MainActivity.class);
                    if(!validateSpaceSign(mOperation.getText().toString()))
                        result = calculator.executeOperation(mOperation.getText().toString().replaceAll(" ", ""));
                    intent.putExtra("section_param", result == null ? 999 : Integer.parseInt(result));
                    startActivity(intent);
                }
            }
        });
    }

    private boolean validate(){
        if(mOperation.getText().toString().isEmpty()){
            mOperation.setError("Debe llenar el campo");
            mOperation.requestFocus();
           return false;
        }

        return true;
    }

    private boolean validateSpaceSign(String input){
        String[] parts = input.split("");
        int countOp = 0;
        for (int i = 0; i<parts.length; i++){
            if(parts[i].equals("-")){
                countOp++;
                if(countOp > 1 && !isNumeric(parts[i])&& (i+2 < parts.length+2) && parts[i+1].equals(" ") &&
                        (isNumeric(parts[i+2]) || parts[i+2].equals("("))){
                    return true;
                }
            }

        }
        return false;
    }

    private boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }





}
