package com.farmatodo.pruebafarmatodo.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.farmatodo.pruebafarmatodo.R;
import com.farmatodo.pruebafarmatodo.adapters.CharacterAdapter;
import com.farmatodo.pruebafarmatodo.adapters.PaginationListener;
import com.farmatodo.pruebafarmatodo.fragments.ListFragment;
import com.farmatodo.pruebafarmatodo.models.Character;
import com.farmatodo.pruebafarmatodo.restApi.ApiClient;
import com.farmatodo.pruebafarmatodo.restApi.ApiInterface;
import com.farmatodo.pruebafarmatodo.restApi.ApiRequestController;
import com.farmatodo.pruebafarmatodo.restApi.Config;
import com.farmatodo.pruebafarmatodo.restApi.DatalistChararters;
import com.farmatodo.pruebafarmatodo.restApi.ListResponse;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(getIntent()!=null){
            ListFragment fragment = new ListFragment();
            fragment.setArguments(getIntent().getExtras());
            loadFragment(fragment, false);
        }

        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    void loadFragment(Fragment fragment, Boolean addToBack){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(addToBack) transaction.addToBackStack(null);
        transaction.replace(R.id.content, fragment);
        transaction.commit();
    }


}
