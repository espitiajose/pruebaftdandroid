package com.farmatodo.pruebafarmatodo.adapters;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.farmatodo.pruebafarmatodo.models.Character;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.farmatodo.pruebafarmatodo.R;

import java.util.ArrayList;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder> {

    private Context context;
    private ArrayList<Character> items;
    private final OnItemClickListener listener;
    private int selectedPos = RecyclerView.NO_POSITION;

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

    public class CharacterViewHolder extends RecyclerView.ViewHolder {

        private ConstraintLayout parent;
        private TextView mName, mDesc, mId;
        private ImageView mImage;


        CharacterViewHolder(View itemView) {
            super(itemView);
            parent = (ConstraintLayout) itemView;
            mName = itemView.findViewById(R.id.name);
            mDesc = itemView.findViewById(R.id.description);
            mId = itemView.findViewById(R.id.idNumber);
            mImage = itemView.findViewById(R.id.image);
        }

        void setSelectedPosition()
        {
            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @SuppressLint("SetTextI18n")
        void bindNews(final int pos, final OnItemClickListener listener) {
            Character item = items.get(pos);
            mName.setText(item.getName());
            mDesc.setText(item.getDescription()!=null && item.getDescription().length() > 0 ? item.getDescription() : "No description");
            mId.setText("ID "+item.getId());
            Glide.with(context).load(item.getThumbnail().getPath()+"."+item.getThumbnail().getExtension())
                    .apply(RequestOptions.circleCropTransform()).error(context.getDrawable(R.drawable.icon)).into(mImage);
            parent.setOnClickListener(v -> {
                listener.onItemClick(item, pos);
                setSelectedPosition();
            });
        }

    }

    public CharacterAdapter(Context context, ArrayList<Character> items, OnItemClickListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Character item, int pos);
    }


    @Override
    public CharacterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View itemView;
        itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_character, viewGroup, false);
        return new CharacterViewHolder(itemView);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(CharacterViewHolder viewHolder, int pos) {

        viewHolder.itemView.setSelected(selectedPos == pos);
        viewHolder.bindNews(pos, listener);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public Character getSelectedItem(){
        if(selectedPos != RecyclerView.NO_POSITION){
            return items.get(selectedPos);
        }else{
            return null;
        }
    }

    public void addItems(ArrayList<Character> postItems) {
        items.addAll(postItems);
        notifyDataSetChanged();
    }
}
