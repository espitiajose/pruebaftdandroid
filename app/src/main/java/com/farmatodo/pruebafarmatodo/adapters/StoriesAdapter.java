package com.farmatodo.pruebafarmatodo.adapters;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.farmatodo.pruebafarmatodo.R;
import com.farmatodo.pruebafarmatodo.models.Comic;
import com.farmatodo.pruebafarmatodo.models.Storie;

import java.util.ArrayList;

public class StoriesAdapter extends RecyclerView.Adapter<StoriesAdapter.StorieViewHolder> {

    private Context context;
    private ArrayList<Storie> items;
    private final OnItemClickListener listener;
    private int selectedPos = RecyclerView.NO_POSITION;

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

    public class StorieViewHolder extends RecyclerView.ViewHolder {

        private ConstraintLayout parent;
        private TextView mName, mId;

        StorieViewHolder(View itemView) {
            super(itemView);
            parent = (ConstraintLayout) itemView;
            mName = itemView.findViewById(R.id.name);
            mId = itemView.findViewById(R.id.idNumber);
        }

        void setSelectedPosition()
        {
            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @SuppressLint("SetTextI18n")
        void bindNews(final int pos, final OnItemClickListener listener) {
            Storie item = items.get(pos);
            mName.setText(item.getTitle());
            mId.setText("ID "+item.getId());
            parent.setOnClickListener(v -> {
                listener.onItemClick(item, pos);
                setSelectedPosition();
            });
        }

    }

    public StoriesAdapter(Context context, ArrayList<Storie> items, OnItemClickListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Storie item, int pos);
    }


    @Override
    public StorieViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View itemView;
        itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_storie, viewGroup, false);
        return new StorieViewHolder(itemView);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(StorieViewHolder viewHolder, int pos) {

        viewHolder.itemView.setSelected(selectedPos == pos);
        viewHolder.bindNews(pos, listener);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public Storie getSelectedItem(){
        if(selectedPos != RecyclerView.NO_POSITION){
            return items.get(selectedPos);
        }else{
            return null;
        }
    }

    public void addItems(ArrayList<Storie> postItems) {
        items.addAll(postItems);
        notifyDataSetChanged();
    }
}
