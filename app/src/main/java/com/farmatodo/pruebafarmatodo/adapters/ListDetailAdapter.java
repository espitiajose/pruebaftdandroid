package com.farmatodo.pruebafarmatodo.adapters;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.farmatodo.pruebafarmatodo.R;
import com.farmatodo.pruebafarmatodo.models.ItemRelationship;

import java.util.ArrayList;

public class ListDetailAdapter extends RecyclerView.Adapter<ListDetailAdapter.ListDetailViewHolder> {

    private Context context;
    private ArrayList<ItemRelationship> items;
    private int selectedPos = RecyclerView.NO_POSITION;

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

    public class ListDetailViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout parent;
        private TextView mName;


        ListDetailViewHolder(View itemView) {
            super(itemView);
            parent = (LinearLayout) itemView;
            mName = itemView.findViewById(R.id.name);
        }

        void setSelectedPosition()
        {
            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);
        }


        void bindNews(final int pos) {
            ItemRelationship item = items.get(pos);
            mName.setText(item.getName());
        }

    }

    public ListDetailAdapter(Context context, ArrayList<ItemRelationship> items) {
        this.context = context;
        this.items = items;
    }

    public interface OnItemClickListener {
        void onItemClick(Character item, int pos);
    }


    @Override
    public ListDetailViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View itemView;
        itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_grid, viewGroup, false);
        return new ListDetailViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ListDetailViewHolder viewHolder, int pos) {

        viewHolder.itemView.setSelected(selectedPos == pos);
        viewHolder.bindNews(pos);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public ItemRelationship getSelectedItem(){
        if(selectedPos != RecyclerView.NO_POSITION){
            return items.get(selectedPos);
        }else{
            return null;
        }
    }
}
