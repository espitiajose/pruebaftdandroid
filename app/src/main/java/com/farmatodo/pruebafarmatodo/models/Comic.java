package com.farmatodo.pruebafarmatodo.models;

import java.io.Serializable;

public class Comic implements Serializable {

    private int id;
    private String title;
    private String description;
    private Thumbnail thumbnail;
    private Relationship creators;

    public Comic(int id, String title, String description, Thumbnail thumbnail, Relationship creators) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.thumbnail = thumbnail;
        this.creators = creators;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Relationship getCreators() {
        return creators;
    }
}
