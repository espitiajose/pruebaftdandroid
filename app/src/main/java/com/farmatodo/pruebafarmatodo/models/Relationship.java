package com.farmatodo.pruebafarmatodo.models;

import java.util.ArrayList;

public class Relationship {

    private int available;
    private String collectionURI;
    private ArrayList<ItemRelationship> items;

    public Relationship(int available, String collectionURI, ArrayList<ItemRelationship> items) {
        this.available = available;
        this.collectionURI = collectionURI;
        this.items = items;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public String getCollectionURI() {
        return collectionURI;
    }

    public void setCollectionURI(String collectionURI) {
        this.collectionURI = collectionURI;
    }

    public ArrayList<ItemRelationship> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemRelationship> items) {
        this.items = items;
    }
}
