package com.farmatodo.pruebafarmatodo.models;

import java.io.Serializable;

public class Storie implements Serializable {

    private int id;
    private String title;
    private String description;
    private Relationship series;

    public Storie(int id, String title, String description, Relationship creators) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.series = creators;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Relationship getSeries() {
        return series;
    }
}
