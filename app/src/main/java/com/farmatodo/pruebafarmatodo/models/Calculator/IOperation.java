package com.farmatodo.pruebafarmatodo.models.Calculator;

import java.math.BigDecimal;


public interface IOperation {
    public BigDecimal operation(BigDecimal firstNumber, BigDecimal secondNumber);
}
