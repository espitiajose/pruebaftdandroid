package com.farmatodo.pruebafarmatodo.models;

import java.io.Serializable;

public class Character implements Serializable {

    private int id;
    private String name;
    private String description;
    private Thumbnail thumbnail;
    private Relationship comics;

    public Character(int id, String name, String description, Thumbnail thumbnail, Relationship comics) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.thumbnail = thumbnail;
        this.comics = comics;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Relationship getComics() {
        return comics;
    }

    public void setComics(Relationship comics) {
        this.comics = comics;
    }
}
