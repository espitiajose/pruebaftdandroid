package com.farmatodo.pruebafarmatodo.models.Calculator;

import java.math.BigDecimal;

public class Subtract implements IOperation {
    @Override
    public BigDecimal operation(BigDecimal firstNumber, BigDecimal secondNumber) {
        return firstNumber.subtract(secondNumber);
    }
}
