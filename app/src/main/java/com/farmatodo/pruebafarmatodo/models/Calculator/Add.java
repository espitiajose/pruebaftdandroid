package com.farmatodo.pruebafarmatodo.models.Calculator;

import java.math.BigDecimal;

public class Add implements IOperation {
    @Override
    public BigDecimal operation(BigDecimal firstNumber, BigDecimal secondNumber) {
        return firstNumber.add(secondNumber);
    }
}
