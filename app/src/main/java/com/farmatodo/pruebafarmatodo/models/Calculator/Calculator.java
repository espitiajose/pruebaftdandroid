package com.farmatodo.pruebafarmatodo.models.Calculator;

import java.util.StringTokenizer;

public class Calculator {

    public static final int CHARACTER_NOT_OCCUR = -1;
    public static final int CHARACTER_NEGATIVE_NUMBER = 0;

    public String executeOperation(String input){
        return selectOperation(input);
    }

    private String selectOperation(String input) {
        while (anyOperation(input)) {
            if (existParenthesis(input)) {
                OperatorParenthesis operation = new OperatorParenthesis(input);
                input = operation.result();
            }else if(existMultiply(input) || existDivide(input)){
                OperationWithPriority operation = new OperationWithPriority(input);
                input = operation.result();
            }else{
                OperatorSimple operation = new OperatorSimple(input);
                input = operation.result();
            }
        }
        return deleteDecimalZero(input);
    }

    private String deleteDecimalZero(String entrada) {
        if(entrada.contains(".")){
            StringTokenizer str = new StringTokenizer(entrada, ".");
            String integerPart = str.nextToken();
            String decimalPart = str.nextToken();
            if("0".equals(decimalPart)){
                return integerPart;
            }
        }
        return entrada;
    }

    private boolean existParenthesis(String input) {
        int result = input.indexOf("(");
        if(result != CHARACTER_NOT_OCCUR){
            return  true;
        }
        return false;
    }

    private boolean anyOperation (String input){
        if(existAdd(input) || existSubtract(input) || existMultiply(input) || existDivide(input)){
            return true;
        }
        return false;
    }

    private boolean existDivide(String input) {
        int result = input.indexOf("/");
        if(result != CHARACTER_NOT_OCCUR){
            return  true;
        }
        return false;
    }

    private boolean existMultiply(String input) {
        int result = input.indexOf("*");
        if(result != CHARACTER_NOT_OCCUR){
            return  true;
        }
        return false;
    }

    private boolean existSubtract(String input) {
        int result = input.lastIndexOf("-");
        if(result != CHARACTER_NOT_OCCUR && result != CHARACTER_NEGATIVE_NUMBER){
            return  true;
        }
        return false;
    }

    private boolean existAdd(String input) {
        int result = input.indexOf("+");
        if(result != CHARACTER_NOT_OCCUR){
            return  true;
        }
        return false;
    }

}
