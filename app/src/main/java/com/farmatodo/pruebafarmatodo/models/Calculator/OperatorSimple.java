package com.farmatodo.pruebafarmatodo.models.Calculator;

import android.util.Log;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

public class OperatorSimple {

    private BigDecimal firstNumber;
    private BigDecimal secondNumber;
    private String operator;
    private DecimalFormat decimalFormat;

    public OperatorSimple(String input) {
        StringTokenizer str = new StringTokenizer(input, "+-*/");
        defineDecimalFormat();
        try{
            if(isFirstNumerNegative(input)){
                this.firstNumber = (BigDecimal) decimalFormat.parse("-" + str.nextToken());
            }else{
                this.firstNumber = (BigDecimal) decimalFormat.parse(str.nextToken());
            }
            this.secondNumber = (BigDecimal) decimalFormat.parse(str.nextToken());
            this.operator = extractOperator(input);
        }catch (ParseException e){
            e.printStackTrace();
        }
    }

    private void defineDecimalFormat() {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        symbols.setDecimalSeparator('.');
        String pattern = "#,##0.0#";
        decimalFormat = new DecimalFormat(pattern, symbols);
        decimalFormat.setParseBigDecimal(true);
    }

    private boolean isFirstNumerNegative(String input) {
        return "-".equals(input.substring(0, 1));
    }

    private String extractOperator(String input) {
        ArrayList<String> operators = new ArrayList<>();
        for(int cont = 1;cont<=input.length();cont++){
            try{
                Integer.parseInt(input.substring(cont-1,cont));
            }catch (NumberFormatException e){
                String character = input.substring(cont-1,cont);
                if((!"-".equals(character) || cont-1 != 0) && !isPoint(character)){
                    operators.add(input.substring(cont-1,cont));
                }
            }
        }

        return operateSigns(operators);
    }

    private boolean isPoint(String character) {
        return ".".equals(character);
    }

    public String result() {
        HashMap<String,IOperation> mapOperations = inicialiceMap();
        IOperation operation = mapOperations.get(this.operator);
        return operation.operation(this.firstNumber,this.secondNumber).toString();
    }

    private HashMap<String, IOperation> inicialiceMap() {
        HashMap<String,IOperation> mapOperations = new HashMap<String, IOperation>();
        mapOperations.put("+", new Add());
        mapOperations.put("-", new Subtract());
        mapOperations.put("*", new Multiply());
        mapOperations.put("/", new Divide());
        return mapOperations;
    }

    private String operateSigns(ArrayList<String> operators){
        String operator = "";
        for (int i=0; i<operators.size(); i++){
            if(i==0){
                operator = operators.get(i);
            }else{
                if(operator.equals("+") && operators.get(i).equals("+")){
                    operator = "+";
                }else if(operator.equals("+") && operators.get(i).equals("-")){
                    operator = "-";
                }else if(operator.equals("-") && operators.get(i).equals("+")){
                    operator = "-";
                }else if(operator.equals("-") && operators.get(i).equals("-")){
                    operator = "+";
                }
            }
        }
        return operator;
    }


}

