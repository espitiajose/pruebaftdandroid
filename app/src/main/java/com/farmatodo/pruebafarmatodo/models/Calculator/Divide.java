package com.farmatodo.pruebafarmatodo.models.Calculator;

import java.math.BigDecimal;

public class Divide implements IOperation {
    @Override
    public BigDecimal operation(BigDecimal firstNumber, BigDecimal secondNumber) {
        return firstNumber.divide(secondNumber);
    }
}
