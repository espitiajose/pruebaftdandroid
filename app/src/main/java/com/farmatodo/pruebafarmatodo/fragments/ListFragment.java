package com.farmatodo.pruebafarmatodo.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.farmatodo.pruebafarmatodo.R;
import com.farmatodo.pruebafarmatodo.adapters.CharacterAdapter;
import com.farmatodo.pruebafarmatodo.adapters.ComicAdapter;
import com.farmatodo.pruebafarmatodo.adapters.PaginationListener;
import com.farmatodo.pruebafarmatodo.adapters.StoriesAdapter;
import com.farmatodo.pruebafarmatodo.models.Character;
import com.farmatodo.pruebafarmatodo.models.Comic;
import com.farmatodo.pruebafarmatodo.models.Storie;
import com.farmatodo.pruebafarmatodo.restApi.ApiRequestController;
import com.farmatodo.pruebafarmatodo.restApi.Config;
import com.farmatodo.pruebafarmatodo.restApi.DataListComics;
import com.farmatodo.pruebafarmatodo.restApi.DataListStories;
import com.farmatodo.pruebafarmatodo.restApi.DatalistChararters;
import com.farmatodo.pruebafarmatodo.restApi.ListResponse;
import com.farmatodo.pruebafarmatodo.restApi.ListResponseCharacter;
import com.farmatodo.pruebafarmatodo.restApi.ListResponseComic;
import com.farmatodo.pruebafarmatodo.restApi.ListResponseStories;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {

    private TextView mTitle;
    private EditText mInputSerach;
    private ImageButton mCancelSearch;
    private RecyclerView mList;
    private ShimmerFrameLayout mShimmerContainer, mShimmerContainerAdd;
    private ApiRequestController mApiRequestController;
    private int offset = 0;
    int param = 0;

    public ListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list, container, false);
        mApiRequestController = ApiRequestController.getInstance();
        mTitle = v.findViewById(R.id.title);
        mList = v.findViewById(R.id.list);
        mInputSerach = v.findViewById(R.id.inputSearch);
        mCancelSearch = v.findViewById(R.id.cancelSearch);
        mShimmerContainer = v.findViewById(R.id.shimmerContainer);
        mShimmerContainerAdd = v.findViewById(R.id.shimmerContainerAdd);

        if(getArguments()!=null){
            param = getArguments().getInt("section_param");
            loadData(param, false, "");
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mList.setLayoutManager(layoutManager);

        mList.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                offset++;
                loadData(param, true, "");
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

        mInputSerach.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    loadData(param, false, mInputSerach.getText().toString());
                    return true;
                }
                return false;
            }
        });
        mInputSerach.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                if(!mInputSerach.getText().toString().isEmpty()){
                    mCancelSearch.setVisibility(View.VISIBLE);
                }else{
                    mInputSerach.setText("");
                    loadData(param, false, "");
                    mCancelSearch.setVisibility(View.INVISIBLE);
                }
            }
        });
        mCancelSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInputSerach.setText("");
                loadData(param, false, "");
                mCancelSearch.setVisibility(View.INVISIBLE);
            }
        });
        return v;
    }

    public void loadData(int param, boolean add, String search){
        Map<String, String> map = Config.getMap();
        map.put("offset", String.valueOf(offset * 20));
        if(add){
            mShimmerContainerAdd.setVisibility(View.VISIBLE);
        }else{
            mList.setVisibility(View.GONE);
            mShimmerContainer.setVisibility(View.VISIBLE);
        }
        switch (param){
            case 0:
                mTitle.setText("Characters");
                if(!search.equals("")) map.put("nameStartsWith", search);
                Call<ListResponseCharacter> request = mApiRequestController.getApiInterface().characters(map);
                request.enqueue(new Callback<ListResponseCharacter>() {
                    @Override
                    public void onResponse(@NonNull Call<ListResponseCharacter> call, @NonNull Response<ListResponseCharacter> response) {
                        ListResponseCharacter serverResponse = response.body();
                        if (serverResponse != null && serverResponse.getCode() == 200) {
                            DatalistChararters data = serverResponse.getData();
                            ArrayList<Character> characters = data.getResults();
                            if(!add){
                                CharacterAdapter adapter = new CharacterAdapter(getContext(), characters, (item, pos) -> {
                                    DetailFragment fragment = new DetailFragment();
                                    Bundle bundle = getArguments();
                                    Objects.requireNonNull(bundle).putSerializable("object", item);
                                    fragment.setArguments(bundle);
                                    loadFragment(fragment, true);
                                });
                                mList.setAdapter(adapter);
                                mShimmerContainer.setVisibility(View.GONE);
                                mList.setVisibility(View.VISIBLE);
                            }{
                                mInputSerach.setVisibility(View.VISIBLE);
                                ((CharacterAdapter) Objects.requireNonNull(mList.getAdapter())).addItems(characters);
                                mShimmerContainerAdd.setVisibility(View.GONE);
                            }

                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ListResponseCharacter> call, @NonNull Throwable t) {
                        t.printStackTrace();
                    }
                });
                break;
            case 3:
            case 5:
                mTitle.setText("Comics");
                if(!search.equals("")) map.put("titleStartsWith", search);
                Call<ListResponseComic> requestComic = mApiRequestController.getApiInterface().comics(map);
                requestComic.enqueue(new Callback<ListResponseComic>() {
                    @Override
                    public void onResponse(@NonNull Call<ListResponseComic> call, @NonNull Response<ListResponseComic> response) {
                        ListResponseComic serverResponse = response.body();
                        if (serverResponse != null && serverResponse.getCode() == 200) {
                            DataListComics data = serverResponse.getData();
                            ArrayList<Comic> comics = data.getResults();
                            if(!add){
                                ComicAdapter adapter = new ComicAdapter(getContext(), comics, (item, pos) -> {
                                    DetailFragment fragment = new DetailFragment();
                                    Bundle bundle = getArguments();
                                    Objects.requireNonNull(bundle).putSerializable("object", item);
                                    fragment.setArguments(bundle);
                                    loadFragment(fragment, true);
                                });
                                mList.setAdapter(adapter);
                                mShimmerContainer.setVisibility(View.GONE);
                                mList.setVisibility(View.VISIBLE);
                            }{
                                mInputSerach.setVisibility(View.VISIBLE);
                                ((ComicAdapter) Objects.requireNonNull(mList.getAdapter())).addItems(comics);
                                mShimmerContainerAdd.setVisibility(View.GONE);
                            }

                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ListResponseComic> call, @NonNull Throwable t) {
                        t.printStackTrace();
                    }
                });
                break;
                default:
                    mTitle.setText("Stories");
                    if(!search.equals("")) map.put("titleStartsWith", search);
                    Call<ListResponseStories> requestStories = mApiRequestController.getApiInterface().stories(map);
                    requestStories.enqueue(new Callback<ListResponseStories>() {
                        @Override
                        public void onResponse(@NonNull Call<ListResponseStories> call, @NonNull Response<ListResponseStories> response) {
                            ListResponseStories serverResponse = response.body();
                            if (serverResponse != null && serverResponse.getCode() == 200) {
                                DataListStories data = serverResponse.getData();
                                ArrayList<Storie> stories = data.getResults();
                                if(!add){
                                    StoriesAdapter adapter = new StoriesAdapter(getContext(), stories, (item, pos) -> {
                                        DetailFragment fragment = new DetailFragment();
                                        Bundle bundle = getArguments();
                                        Objects.requireNonNull(bundle).putSerializable("object", item);
                                        fragment.setArguments(bundle);
                                        loadFragment(fragment, true);
                                    });
                                    mList.setAdapter(adapter);
                                    mShimmerContainer.setVisibility(View.GONE);
                                    mList.setVisibility(View.VISIBLE);
                                }{
                                    mInputSerach.setVisibility(View.VISIBLE);
                                    ((StoriesAdapter) Objects.requireNonNull(mList.getAdapter())).addItems(stories);
                                    mShimmerContainerAdd.setVisibility(View.GONE);
                                }

                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<ListResponseStories> call, @NonNull Throwable t) {
                            t.printStackTrace();
                        }
                    });
                    break;
        }
    }

    void loadFragment(Fragment fragment, Boolean addToBack){
        FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(addToBack) transaction.addToBackStack(null);
        transaction.add(R.id.content, fragment);
        transaction.commit();
    }

}
