package com.farmatodo.pruebafarmatodo.fragments;


import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.farmatodo.pruebafarmatodo.R;
import com.farmatodo.pruebafarmatodo.adapters.ListDetailAdapter;
import com.farmatodo.pruebafarmatodo.models.Character;
import com.farmatodo.pruebafarmatodo.models.Comic;
import com.farmatodo.pruebafarmatodo.models.Storie;

import java.io.Serializable;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment {

    private ImageView mCoverPage;
    private TextView mTitle, mDescription, mAssociatedText;
    private RecyclerView mListDetail;


    public DetailFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detail, container, false);
        mCoverPage = v.findViewById(R.id.coverPage);
        mTitle = v.findViewById(R.id.title);
        mDescription = v.findViewById(R.id.description);
        mAssociatedText = v.findViewById(R.id.associatedText);
        mListDetail = v.findViewById(R.id.listDetail);

        GridLayoutManager gridManager = new GridLayoutManager(getContext(),2);
        mListDetail.setLayoutManager(gridManager);

        if(getArguments()!=null){
            loadData(getArguments().getInt("section_param"), getArguments().getSerializable("object"));
        }
        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    void loadData(int param, Serializable serializable){
        switch (param){
            case 0:
                Character character = (Character) serializable;
                Glide.with(this).load(character.getThumbnail().getPath()+"."+character.getThumbnail().getExtension())
                        .apply(RequestOptions.centerCropTransform()).error(getResources().getDrawable(R.drawable.icon)).into(mCoverPage);
                mTitle.setText(character.getName());
                mAssociatedText.setText("Associated Comics");
                mDescription.setText(character.getDescription()!=null && character.getDescription().length() > 0 ?
                        Html.fromHtml(character.getDescription(), Html.FROM_HTML_MODE_COMPACT) : "No description to display");
                mListDetail.setAdapter(new ListDetailAdapter(getContext(), character.getComics().getItems()));
                break;
            case 3:
            case 5:
                Comic comic = (Comic) serializable;
                Glide.with(this).load(comic.getThumbnail().getPath()+"."+comic.getThumbnail().getExtension())
                        .apply(RequestOptions.centerCropTransform()).error(getResources().getDrawable(R.drawable.icon)).into(mCoverPage);
                mTitle.setText(comic.getTitle());
                mAssociatedText.setText("Creators");
                mDescription.setText(comic.getDescription()!=null && comic.getDescription().length() > 0 ?
                        Html.fromHtml(comic.getDescription(), Html.FROM_HTML_MODE_COMPACT) : "No description to display");
                mListDetail.setAdapter(new ListDetailAdapter(getContext(), comic.getCreators().getItems()));
                break;
                default:
                    Storie storie = (Storie) serializable;
                    mCoverPage.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    mTitle.setText(storie.getTitle());
                    mTitle.setTextSize(16);
                    mAssociatedText.setText("Associated Series");
                    mDescription.setText(storie.getDescription()!=null && storie.getDescription().length() > 0 ?
                            Html.fromHtml(storie.getDescription(), Html.FROM_HTML_MODE_COMPACT) : "No description to display");
                    mListDetail.setAdapter(new ListDetailAdapter(getContext(), storie.getSeries().getItems()));
                    break;
        }
    }

}
